variables:
  MAVEN_OPTS: "-Dhttps.protocols=TLSv1.2 -Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
  MAVEN_CLI_OPTS: "--batch-mode --errors --fail-at-end --show-version -DinstallAtEnd=true -DdeployAtEnd=true"
  GIT_STRATEGY: none
  
cache:
  paths:
    - .m2/repository
stages:
  - setup
  - build
  - test
  - review #no jobs have been implemented yet for this stage
  - deploy #no jobs have been implemented yet for this stage
  - dast #no jobs have been implemented yet for this stage
  - performance #no jobs have been implemented yet for this stage

.verify: &verify
  stage: test
  image: maven:3.3.9-jdk-8
  
  script:
    - 'mvn $MAVEN_CLI_OPTS verify'
  rules:
    - if: '$CI_COMMIT_BRANCH != "master"  && $CI_COMMIT_BRANCH'

# Verify merge requests using JDK8
verify:jdk8:
  <<: *verify

# To deploy packages from CI, create a ci_settings.xml file
# For deploying packages to GitLab's Maven Repository: See https://docs.gitlab.com/ee/user/project/packages/maven_repository.html#creating-maven-packages-with-gitlab-cicd for more details.
# Please note: The GitLab Maven Repository is currently only available in GitLab Premium / Ultimate.
# For `master` branch run `mvn deploy` automatically.
mvn_deploy:jdk8:
  stage: build
  image: maven:3.3.9-jdk-8
  script:
    - if [ ! -f ci_settings.xml ];
        then echo "CI settings missing\! If deploying to GitLab Maven Repository, please see https://docs.gitlab.com/ee/user/project/packages/maven_repository.html#creating-maven-packages-with-gitlab-cicd for instructions.";
      fi
    - 'mvn $MAVEN_CLI_OPTS deploy -s ci_settings.xml'
  rules:
    - if: $CI_COMMIT_TAG
#    - if: $CI_COMMIT_BRANCH == "master"

deploy_dev:
  image: maven:3.3.9-jdk-8
  stage: deploy
  before_script: 
    - 'mkdir /builds/juliebyrne-demo/tanuki-tech-spring/deploy_dir'
    - 'ls -la /builds/juliebyrne-demo/tanuki-tech-spring/'
  script:
    - mvn dependency:get -Dartifact=org.springframework:gs-spring-boot:0.1.0
    - cp /builds/juliebyrne-demo/tanuki-tech-spring/.m2/repository/org/springframework/gs-spring-boot/0.1.0/* /builds/juliebyrne-demo/tanuki-tech-spring/deploy_dir/
    - echo "fetched artifacts"
    - ls -la /builds/juliebyrne-demo/tanuki-tech-spring/deploy_dir/*
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
    
deploy_qa:
  stage: deploy
  script: 
    - "echo $CI_COMMIT_TAG"
    - "echo deploying to QA environment"
  rules:
    - if: '$CI_COMMIT_TAG'

#show manual step for deploying to prod after automated deploy to QA
deploy_prod:
  stage: deploy
  script: "echo deploying to prod"
  rules:
    - if: '$CI_COMMIT_BRANCH == "production"'
      when: manual

#to do: alternate method - automate prod deployment when matching tag
